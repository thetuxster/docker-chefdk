# Introduction
Docker image to run chefdk in isolated docker environment.

# Usage
```bash
docker pull josephjones/chefdk
```
1. Create directories for chef workspace
	* .chef
	* cookbooks
	* environments
	* roles
2. copy knife.rb, $org-validator.pem and username.pem to .chef
3. cd to workspace and start up docker environment
4. ```docker run --rm -ti -v $PWD:/data josephjones/chefdk```

# Caveat
the docker image starts with a user named chef-user with UID 1000.
if the user you run the container as doesn't have the same UID then the files
that get created in the container wont' be owned by you
